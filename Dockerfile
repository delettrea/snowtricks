FROM php:7.4-alpine

RUN apk add --no-cache \
        curl \
        postgresql-dev

RUN adduser -D -u 1000 otter
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer


RUN docker-php-ext-configure pgsql -with-pgsql=/usr/include/postgresql/
RUN docker-php-ext-install pdo_pgsql \
    pgsql

COPY . ../snowTricks2

WORKDIR /snowTricks2

CMD php -S 0.0.0.0:80 public/index.php